✔ Dockerfile for your application and any other needed component.

✔ Docker images held in a Docker registry.

✔ Jenkins projects configured to achieve the CI/CD.

✔ Docker Compose to run docker containers.

✔ Deploying the containers on Docker swarm.

## Info
Vagrant levregaing shell scripts to build/provision Docker Swarm cluster that hosts a simple bot application built and replicated on the provisioned Swarm cluster.
The application is a socket server & client ping-pong demo written in Golang.
Jenkins is then used to achieve the CI/CD phase.

## Requirements
- Vagrant >= 2.0.x
- Virtualbox

## How it Works
#### Vagrant:
`CLUSTER_TYPE=swarm vagrant up` to spin up a swarm cluster
`CLUSTER_TYPE=k8s vagrant up` to spin up a k8s cluster

- Master: Swarm/k8s manager
- Node-1: Joined Swarm/k8s worker/node
- Node-2: Joined Swarm/k8s worker/node
- Jenkins: For the CI/CD

#### Jenkins:
Through the included jenkinsfile, and the bash wrapper script "develop", jenkins does the build process/stages as follows;

- Build, excutes `docker-compose up -d --build` to build the docker image.
- Test, excutes `docker-compose exec -T \ app sh -c "go test"` to trigger the app test-unit in a new docker container.
- Push, pushes the image to DockerHub registry(the one defined through jenkins Global Credentials, in Step 3.b)
- Deploy, jenkins then ssh into the Swarm master, to excute 1 replica of the docker service bot-app (saed-bot)
- Clean, excutes `docker-compose down` to bring the down the stack, and clean up.

## How to use

#### 1. Clone the repo
- `git clone git@bitbucket.org:saedabdu/vagrant-swarm-k8s.git`
- `cd vagrant-swarm-k8s`

#### 2. Launch Docker Swarm cluster
- `cd vagrant-swarm-k8s/vagrant`
- `CLUSTER_TYPE=swarm vagrant up master node-2 node-2 jenkins`

#### 3. Jenkins
##### 3.a Initialize Jenkins
- `cd vagrant-swarm-k8s/vagrant`
- `vagrant ssh jenkins`
- `sudo cat /var/lib/jenkins/secrets/intialAdminPassword`
- Firefox http://localhost:8080
- Insert Jenkins password
- Install recommended plugins
- Create a user for your jenkins node.

##### 3.b. Setup Global Credentials
- Jenkins > Credentials > Setup Global Credentials
- Username: Your Docker Registery repo username
- Password: Your Docker Registery repo password
- ID: dockerHub

##### 3.c. Create a build job
- Create Job, Pipeline
- Add jenkinsfile
- Save > Apply

#### 4. Access The App
- http://localhost:8888/ping and should recieve `pong` as a reply.